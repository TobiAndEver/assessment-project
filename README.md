# Assessment Project

In our Farm in Kuwait we would like to use a robot to bring the harvested salad from the harvest station to the packing station. 

* You are given a starting point (x,y) and a direction (N, S, W, E)
* The robot receives a character array of commands.
* Implement commands that move the robot forward/backward (f,b).
* Implement commands that turn the robot left/right (l,r).
* In the Farm there are Farm Workers and obstacles. Implement an Obstacle detection: If a given sequence of commands encounters an obstacle, the rover moves up to the last possible point, aborts the sequence and reports the obstacle.



Please solve that task with TDD and preferable Typescript or python.